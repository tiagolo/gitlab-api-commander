package org.gitlab.api

import br.com.mv.devops.api.BaseAPI
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.gitlab.api.types.BranchAccessLevel

/**
 * Created by tiago.costa on 17/11/2016.
 */
class GitlabAPI extends BaseAPI {

    String gitLabUrl
    String gitLabPrivateToken
    Boolean DEBUG

    def out
    private String privateToken = "private_token=${gitLabPrivateToken}"

    GitlabAPI(String gitLabUrl, String gitLabPrivateToken, Boolean DEBUG = null) {
        super()

        this.gitLabUrl = gitLabUrl
        this.gitLabPrivateToken = gitLabPrivateToken
        this.privateToken = "private_token=${gitLabPrivateToken}"
        this.DEBUG = DEBUG
    }

    def transferProjectToGroup(def projectId, def groupId) {
        def endpoint = "${gitLabUrl}/groups/${groupId.replaceAll("/","%2F")}/projects/${projectId.replaceAll("/","%2F")}"
        def json = callRestApi("$endpoint?$privateToken", "POST")
        new JsonSlurper().parseText(json)
    }

    Collection listUsers(def page = 1) {
        def endpoint = "${gitLabUrl}/users?active=true&page=${page}"
        def url = new URL("$endpoint&$privateToken")
        def json = url.text
        List users = new JsonSlurper().parseText(json)

        def nextPage = url.openConnection().getHeaderField("X-Next-Page")
        if (nextPage)
            users.addAll(listUsers(nextPage))

        users
    }

    def createImpersonationToken(Integer userId) {
        def endpoint = "${gitLabUrl}/users/${userId}/impersonation_tokens"

        def tomorrow = new Date() + 1

        def body = new JsonBuilder()
        body {
            private_token gitLabPrivateToken
            name 'check-status'
            'scopes[]' 'api'
            expires_at tomorrow.format('YYYY-MM-d')
        }
        body = getMessageBody(body)

        def json = callRestApi("$endpoint?$privateToken", "POST", body)
        new JsonSlurper().parseText(json)
    }

    def deleteImpersonationToken(Integer userId, Integer tokenId) {
        def endpoint = "${gitLabUrl}/users/${userId}/impersonation_tokens/${tokenId}"
        callRestApi("$endpoint?$privateToken", "DELETE")
    }

    def deleteMilestore(String gitLabProjectId, String milestone) {
        try {

            def endpoint = "${gitLabUrl}/projects/${gitLabProjectId}/milestones"

            def jsonText = new URL("${endpoint}?search=${milestone}&${privateToken}").text
            def milestoneId = new JsonSlurper().parseText(jsonText).id[0]

            def body = new JsonBuilder()
            body {
                private_token gitLabPrivateToken
                milestone_id milestoneId
            }
            body = getMessageBody(body)


            out.println "GitLab : Creating Milestone :: ${gitLabProjectId.replaceAll("%2F", "/")} :: ${milestone} ::"
            callRestApi("$endpoint/$milestoneId?$privateToken", "DELETE", body)
        } catch (e) {
            out.println "::::::::::::: Não foi possível executar endPoint: $e"
        }
    }

    def createMilestore(String gitLabProjectId, String milestone, String startDate = null, String dueDate = null) {
        def endpointProtected = "${gitLabUrl}/projects/${gitLabProjectId}/milestones"

        def body = new JsonBuilder()
        body {
            private_token gitLabPrivateToken
            title milestone
            start_date startDate
            due_date dueDate
        }
        body = getMessageBody(body)


        try {
            out.println "GitLab : Creating Milestone :: ${gitLabProjectId.replaceAll("%2F", "/")} :: ${milestone} ::"
            callRestApi("$endpointProtected?$privateToken", "POST", body)
        } catch (e) {
            out.println "::::::::::::: Não foi possível executar endPoint: $e"
        }
    }

    def setDefaultBranch(String gitLabProjectId, String branchName) {
        def endpointProtected = "${gitLabUrl}/projects/${gitLabProjectId}/"

        def body = new JsonBuilder()
        body {
            private_token gitLabPrivateToken
            default_branch branchName
        }
        body = getMessageBody(body)


        try {
            out.println "GitLab : Setting Default Branch :: ${gitLabProjectId.replaceAll("%2F", "/")} :: ${branchName} ::"
            callRestApi("$endpointProtected?$privateToken", "PUT", body)
        } catch (e) {
            out.println "::::::::::::: Não foi possível executar endPoint: $e"
        }
    }

    def protectBranch(String gitLabProjectId, String branchName, BranchAccessLevel pushAccessLevel, BranchAccessLevel mergeAccessLevel) {
        protectBranch(gitLabProjectId, branchName, pushAccessLevel.value, mergeAccessLevel.value)
    }

    def protectBranch(String gitLabProjectId, String branchName, int pushAccessLevel, int mergeAccessLevel) {
        def endpointProtected = "${gitLabUrl}/projects/${gitLabProjectId}/protected_branches"

        def body = new JsonBuilder()
        body {
            private_token gitLabPrivateToken
            name branchName
            push_access_level pushAccessLevel
            merge_access_level mergeAccessLevel
        }
        body = getMessageBody(body)


        try {
            unprotectBranch(gitLabProjectId, branchName)

            out.println "GitLab : Protecting Branch :: ${gitLabProjectId.replaceAll("%2F", "/")} :: ${branchName} ::"
            callRestApi("$endpointProtected?$privateToken", "POST", body)
        } catch (e) {
            out.println "::::::::::::: Não foi possível executar endPoint: $e"
        }
    }

    def unprotectBranch(String gitLabProjectId, String branchName) {
        def endpointUnprotected = "${gitLabUrl}/projects/${gitLabProjectId}/protected_branches/${branchName.replaceAll("/","%2F")}"

        out.println "GitLab : Unprotecting Branch :: ${gitLabProjectId.replaceAll("%2F", "/")} :: ${branchName} ::"

        try {
            callRestApi("$endpointUnprotected?$privateToken", "DELETE")
        } catch (e) {
            out.println "::::::::::::: Não foi possível executar endPoint: $e"
        }
    }

    def listGroups() {
        def endpoint = "${gitLabUrl}/groups"
        def json = new URL("$endpoint?$privateToken").text
        new JsonSlurper().parseText(json)
    }

    def createGroup(groupName, groupPath, parentId) {
        def endpoint = "${gitLabUrl}/groups"

        def groupJsonText
        def jsonText = new URL("${endpoint}/${parentId.replaceAll("/", "%2F")}?${privateToken}").text
        def groupParentId = new JsonSlurper().parseText(jsonText).id

        def body = new JsonBuilder()
        body {
            private_token gitLabPrivateToken
            name groupName
            path groupPath
            parent_id groupParentId
        }
        body = getMessageBody(body)

        out.println "GitLab : Create Group :: ${parentId}/${groupPath} :: \n"

        try {
            groupJsonText = callRestApi("${endpoint}?${privateToken}", "POST", body)
        } catch (IOException e) {
            groupJsonText = new URL("${endpoint}/${parentId.replaceAll("/", "%2F")}%2Fdatabase?${privateToken}").text
        }

        return new JsonSlurper().parseText(groupJsonText).id
    }

    def createProject(projectName, projectDescription, projectParentPath, projectParentId) {
        def endpoint = "${gitLabUrl}/projects"
        def projectJsonText

        def body = new JsonBuilder()
        body {
            private_token gitLabPrivateToken
            name projectName
            description projectDescription
            namespace_id projectParentId
        }
        body = getMessageBody(body)

        out.println "GitLab : Create Project :: ${projectParentPath}/${projectName} :: "
        try {
            projectJsonText = callRestApi("${endpoint}?${privateToken}", "POST", body)
        } catch (IOException e) {
            projectJsonText = new URL("${endpoint}/${projectParentPath.replaceAll("/", "%2F")}%2F${projectName}?${privateToken}").text
        }

        return projectJsonText
    }

    /**
     * Cria através da API Rest do gitlab, o serviço de integração entre o
     * bamboo e o gitlab, para a execução de builds no bamboo através dos
     * eventos de git push realizados nos repositórios git.
     *
     * @param buildPlanKey - ID do plano de build para o projeto no bamboo
     * @param gitLabProjectId - ID do projeto no Gitlab
     * @return
     */
    def createBambooService(bambooData, buildPlanKey, gitLabProjectId) {
        def endpoint = "${gitLabUrl}/projects/${gitLabProjectId}/services/bamboo"

        def body = new JsonBuilder()
        body {
            private_token gitLabPrivateToken
            bamboo_url bambooData.url
            build_key buildPlanKey
            username bambooData.username
            password bambooData.password
        }
        body = getMessageBody(body)

        def jsonText = new URL("${endpoint}?${privateToken}").text
        def hooks = new JsonSlurper().parseText(jsonText)

        if (DEBUG) {
            out.println """
DEBUG :: createBambooService
    PATH: ${bambooData.url}
    BUILD-KEY: ${buildPlanKey}
    GITLAB-ID: ${gitLabProjectId}
    BODY: ${body}
    =========================== 1st ============================
    JSON: ${jsonText}
    ============================================================
    """
//        if (hooks.findAll { it.url == webHookUrl }.empty) {
//            out.println "==== Criando WebHook em: $endpoint"
//            out.println callRestApi(endpoint, "PUT", body)
//        }
        }

        out.print "GitLab : Atlassian Bamboo :: ${gitLabProjectId.replaceAll("%2F", "/")} :: "
        out.println callRestApi("${endpoint}?${privateToken}", "PUT", body)
    }

    /**
     * Cria um webhook do jenkins no gitlab através de API Rest. Este webhook é
     * utilizado para notificar tanto o jenkins sobre pushes no gitlab, quando
     * ao gitlab sobre a execução de builds no jenkins. (Use GitLab CI features)
     *
     * Para mais informações:
     * @see <a href="https://github.com/jenkinsci/gitlab-plugin">https://github.com/jenkinsci/gitlab-plugin</a>
     * @see <a href="http://docs.gitlab.com/ee/integration/jenkins.html">http://docs.gitlab.com/ee/integration/jenkins.html</a>
     *
     * @param path
     * @param moduleName
     * @param gitLabProjectId
     * @param branch
     * @return
     */
    def createProjectHook(path, moduleName, gitLabProjectId, branch) {
        def endpoint = "${gitLabUrl}/projects/${gitLabProjectId}/hooks"
        def webHookUrl = "${jenkinsUrl.replaceAll("/\$", "")}/project/${path}${branch}/${moduleName}"
        def body = new JsonBuilder()
        body {
            private_token gitPrivateToken
            url webHookUrl
            push_events true
        }
        body = getMessageBody(body)
        def jsonText = new URL("${endpoint}?${privateToken}").text
        def hooks = new JsonSlurper().parseText(jsonText)
        if (DEBUG) {
            out.println """
DEBUG :: createProjectHook
    PATH: ${path}
    MODULE: ${moduleName}
    WEBHOOK: ${webHookUrl}
    BODY: ${body}
    =========================== 1st ============================
    JSON: ${jsonText}
    ============================================================
    """
        }
        if (hooks.findAll { it.url == webHookUrl }.empty) {
            out.println "==== Criando WebHook em: $endpoint"
            out.println callRestApi(endpoint, "POST", body)
        }
    }
}
