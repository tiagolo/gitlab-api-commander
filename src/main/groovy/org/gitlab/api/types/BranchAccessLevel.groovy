package org.gitlab.api.types

enum BranchAccessLevel {

    NO_ONE(0),
    DEVELOPER(30),
    MASTER(40)

    int value

    BranchAccessLevel(int value) {
        this.value = value
    }

    int getValue(){
        this.value
    }
}