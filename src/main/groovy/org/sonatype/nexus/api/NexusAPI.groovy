package org.sonatype.nexus.api

import br.com.mv.devops.api.BaseAPI
import groovyx.net.http.HttpResponseException
import groovyx.net.http.RESTClient

class NexusAPI extends BaseAPI {

    String nexusUrl
    RESTClient client

    NexusAPI(String nexusUrl) {
        this.nexusUrl = nexusUrl
        this.client = createRestClient(nexusUrl)
    }

    RESTClient createRestClient(String url) {
//        def username = 'admin'
//        def password = 'admin123'
        def username = 'tiago.costa'
        def password = 'bfY!@Voh5'

        RESTClient client = new RESTClient("${url}/service/siesta/rest/")
//        client.setProxy('192.168.254.11', 3128, 'http')
        client.auth.basic(username, password)
        client.headers = [
                'Accept'       : 'application/json',
                'Authorization': 'Basic dGlhZ28uY29zdGE6YmZZIUBWb2g1'
        ]

        return client
    }

    def listComponents(Map query) {
        this.client.get(path: 'beta/search', query: query).data
    }

    def deleteComponent(String id, String name, String version) {
        try {
            println "Component: $name - $version | Start Delete"
            def resp = this.client.delete(path: "beta/components/${id}")

            if (resp.status == 204) {
                println "Component: $name - $version | Successful Deleted"
            }
        } catch (HttpResponseException resp) {
            println "Component: $name - $version | Failed to Delete"
        }
    }
}
