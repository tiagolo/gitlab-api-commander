package br.com.mv.devops.cli

import com.atlassian.api.bamboo.BambooAPI
import groovy.json.JsonSlurper
import org.apache.groovy.json.internal.LazyMap
import org.gitlab.api.GitlabAPI
import org.gitlab.api.types.BranchAccessLevel
import org.sonatype.nexus.api.NexusAPI

class Main {

    def out
    def binding
    GitlabAPI gitLabAPI
    BambooAPI bambooAPI
    NexusAPI nexusAPI

    static void main(String[] args) {
        def main = new Main()
        main.bootstrapGitlabAPI()
//        main.moveOldSaudePublicaProjects()
        main.callGitlabAPI()
//        main.verifyUsersStatus()
//        main.bulkDeleteBambooBranches()
//        main.deleteOldReleaseCandidates()
    }

    def loadJSONData(String fileName) {
        def data = new JsonSlurper().parse(new File("${metadata["workspace"]}/src/main/resources/$fileName"))
        return data
    }

    /**
     * *******************************************************************
     *      Acquiring Variables
     * *******************************************************************
     */
    def getMetadata() {

        def metadata = [
                workspace         : "",
                nexusURL          : "http://nexus.mv.com.br:8082",
                bambooURL         : "https://bamboo.cloudmv.com.br",
                bambooToken       : "d933004d053ca6b566da8354ee941d20839790dc",
                gitlabURL         : "https://git.mv.com.br/api/v4",
                gitlabPrivateToken: "${System.getenv("gitlabPrivateToken")}",
                gitlabUsername    : "bamboo-integration",
                gitlabPassword    : "${System.getenv("gitlabPassword")}"
        ]

        metadata["workspace"] = getClass().protectionDomain.codeSource.location.path - ~/src\/.*/

        if (["/groovy/script"].any { metadata['workspace'].contains(it) })
            metadata["workspace"] = "/D:/MV/git/devops/bamboo/bamboo-plan-dsl"
        else if ((metadata["workspace"]).endsWith("out/production/classes/"))
            metadata["workspace"] -= "out/production/classes/"
        else
            out.println metadata["workspace"]

        return metadata
    }


    void deleteOldReleaseCandidates(String continuationToken = null) {
        def queryParameters = [
                repository: "maven-releases",
//                q: '1.48.0'
                q: 'br.com.mv.soul'
        ]

        if (continuationToken) queryParameters.put(
                continuationToken, continuationToken)

        def resp = nexusAPI.listComponents(queryParameters)
        println "Quantidade de itens a serem removidos: ${resp.items.size}"
        resp.items.each {
            if (!(it.version.endsWith("RELEASE")) && it.group.startsWith("br.com.mv")) {
                nexusAPI.deleteComponent(it.id, it.name, it.version)
            }
        }

        if (resp.continuationToken)
            deleteOldReleaseCandidates()
    }

    def bulkDeleteBambooBranches() {
        def plans = bambooAPI.listBuildPlans()
        plans.plans.plan.each { plan ->
            def branches = bambooAPI.getBuildPlanBranches(plan.key)
            branches.branches.branch.each { branch ->
                if (branch.shortName.startsWith("hotfix") ||
                        branch.shortName.startsWith("release")) {
                    bambooAPI.deleteBuildPlan(branch.key)
                }
            }
        }
    }

    def verifyUsersStatus() {
        bootstrapGitlabAPI()

        print ":::: Fetching Users: "
        def users = gitLabAPI.listUsers()

        println "Total Count - ${users.size()}"

        users.eachWithIndex { user, i ->
            try {
                checkUserStatus(user.id)
                println ":::: ${i.toString().padLeft(4,'0')} User: ${user.name} - Active"
            } catch (e) {
                println ":::: ${i.toString().padLeft(4,'0')} User: ${user.name} - Blocked by LDAP"
            }
        }
    }

    private void checkUserStatus(Integer userId) {
        def impersonationToken = gitLabAPI.createImpersonationToken(userId)
        GitlabAPI checkUserApi = new GitlabAPI(metadata["gitlabURL"], impersonationToken.token)
        checkUserApi.listGroups()
//        gitLabAPI.deleteImpersonationToken(userId, (Integer) impersonationToken.id)
    }

    def callGitlabAPI() {
        bootstrapGitlabAPI()

        LazyMap data = loadJSONData("soul-products-html5.json")
//        LazyMap data = loadJSONData("devops.json")
        data.projects.each { project ->
            protectSystemBranches(project)
        }
    }

    def moveOldSaudePublicaProjects() {
        bootstrapGitlabAPI()

        ["cds","client-cns-betim","cadastros-basicos-flex", "relatorios_sysreport", "cadastros-basicos-app"].each { project ->
            println gitLabAPI.transferProjectToGroup("saude-publica/${project}", "saude-publica-legacy/archive")
        }
    }

    private void bootstrapGitlabAPI() {
        getMetadata()
        gitLabAPI = gitLabAPI ?: new GitlabAPI(metadata["gitlabURL"], metadata["gitlabPrivateToken"])
        bambooAPI = bambooAPI ?: new BambooAPI(metadata["bambooURL"], metadata["bambooToken"])
        nexusAPI = nexusAPI ?: new NexusAPI(metadata["nexusURL"])
    }

    private protectSystemBranches(project) {
        project.plans.each { system ->
//            if (!["PRODUCT","PARENT"].contains(system.key)) {
            if (![].contains(system.key)) {
//            if (!["PARENT"].contains(system.key)) {
                gitflowAccessLevel("${system.gitNamespace}/${system.gitRepo}".replaceAll("/","%2F"))
                gitflowAccessLevel("${system.gitNamespace}/database/${system.gitRepo}".replaceAll("/", "%2F"))
                gitflowAccessLevel("${system.gitNamespace.replace("/","mv-")}/${system.gitRepo}".replaceAll("/","%2F"))
            }
        }
    }

    private void gitflowAccessLevel(String gitlabProjectId) {
//        openGitflowAccessLevel(gitlabProjectId)
        closeGitflowAccessLevel(gitlabProjectId)
        gitLabAPI.setDefaultBranch(gitlabProjectId, "develop")
//        gitLabAPI.deleteMilestore(gitlabProjectId, "v2019.1.0")
    }

    private void openGitflowAccessLevel(String gitlabProjectId) {
//        gitLabAPI.protectBranch(gitlabProjectId, "master", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
//        gitLabAPI.protectBranch(gitlabProjectId, "develop", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
//        gitLabAPI.protectBranch(gitlabProjectId, "release/20*", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
//        gitLabAPI.protectBranch(gitlabProjectId, "hotfix/20*", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
//        gitLabAPI.unprotectBranch(gitlabProjectId, "release/20*")
//        gitLabAPI.unprotectBranch(gitlabProjectId, "hotfix/20*")
//        gitLabAPI.unprotectBranch(gitlabProjectId, "develop")
        gitLabAPI.unprotectBranch(gitlabProjectId, "master")
//        gitLabAPI.unprotectBranch(gitlabProjectId, "cllient")
    }

    private void closeGitflowAccessLevel(String gitlabProjectId) {
        gitLabAPI.protectBranch(gitlabProjectId, "client/*", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
        gitLabAPI.protectBranch(gitlabProjectId, "master", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
        gitLabAPI.protectBranch(gitlabProjectId, "LTS", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
        gitLabAPI.protectBranch(gitlabProjectId, "develop", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
        gitLabAPI.protectBranch(gitlabProjectId, "release/20*", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
        gitLabAPI.protectBranch(gitlabProjectId, "hotfix/20*", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
        gitLabAPI.protectBranch(gitlabProjectId, "internacional/20*", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
        gitLabAPI.protectBranch(gitlabProjectId, "baseline/internacional", BranchAccessLevel.MASTER, BranchAccessLevel.MASTER)
    }

    private void createDatabaseProject(project) {
        def groupId = gitLabAPI.createGroup("database", "database", project.plans.first().gitNamespace)
        project.plans.each { system ->
            if (!["PRODUCT", "PARENT"].contains(system.key)) {
                def systemJsonText = gitLabAPI.createProject(system.gitRepo, system.name, "${system.gitNamespace}/database", groupId)
                def systemDB = new JsonSlurper().parseText(systemJsonText)þ

                out.println systemJsonText

                def gitBaseCMD = "git -C D:/MV/git/soulmv/database"
                RunCMD("$gitBaseCMD remote add ${systemDB.name} ${systemDB.ssh_url_to_repo}")
                RunCMD("$gitBaseCMD push ${systemDB.name} --all")
                //systemDB.ssh_url_to_repo
            }
        }
    }

    private void RunCMD(String CMD) {
        def proc = CMD.execute()
        def sout = new StringBuilder(), serr = new StringBuilder()
        proc.consumeProcessOutput(sout, serr)
        proc.waitFor()
        out.println "out> ${sout} err> ${serr}"
    }

}
