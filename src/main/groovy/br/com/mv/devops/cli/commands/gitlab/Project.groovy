package br.com.mv.devops.cli.commands.gitlab

import br.com.mv.devops.cli.commands.GitlabCLI
import br.com.mv.devops.cli.commands.base.AbstractCommand
import picocli.CommandLine

@CommandLine.Command(name = "project",
        subcommands = [Milestone.class, ProtectedBranches.class])
class Project extends AbstractCommand {

    @CommandLine.Parameters(arity = "1")
    String projectId

    @Override
    void run() {
        super.run()
        CommandLine.usage(this, System.out)
    }

    @CommandLine.Command(name = "default-branch")
    void setDefaultBranch(String branch) {

    }
}

@CommandLine.Command(name = "protected-branches")
class ProtectedBranches extends AbstractCommand {
    @Override
    void run() {
        super.run()
        CommandLine.usage(this, System.out)
    }

    void close(){}
}

@CommandLine.Command(name = "milestone")
class Milestone extends AbstractCommand{

    @Override
    void run() {
        CommandLine.usage(this, System.out)
    }

    @CommandLine.Command
    void create(@CommandLine.Parameters(paramLabel = "milestone") String milestone) {
        println(milestone)
    }

    @CommandLine.Command
    void edit(@CommandLine.Parameters(paramLabel = "milestone") String milestone) {
        println(milestone)
    }

    @CommandLine.Command
    void delete(@CommandLine.Parameters(paramLabel = "milestone") String milestone) {
        println(milestone)
    }
}