package br.com.mv.devops.cli.commands.base


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import picocli.CommandLine

@CommandLine.Command(
        headerHeading = "%n",
        descriptionHeading = "%n",
        parameterListHeading = "%nParameters:%n",
        optionListHeading = "%nOptions:%n",
        commandListHeading = "%nCommands:%n",
        addMethodSubcommands = false)
abstract class AbstractCommand implements Runnable {

    @CommandLine.Option(names = ["-h", "--help"], usageHelp = true, description = "display this help message")
    boolean isUsageHelpRequested

    @CommandLine.ParentCommand
    AbstractCommand parent

    protected Logger logger

    @Override
    void run() {
    }

    AbstractCommand() {
        logger = LoggerFactory.getLogger("DevopsCLI")
    }
}
