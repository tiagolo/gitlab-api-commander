package br.com.mv.devops.cli.commands

import br.com.mv.devops.cli.commands.base.AbstractCommand
import br.com.mv.devops.cli.commands.gitlab.Project
import org.gitlab.api.GitlabAPI
import picocli.CommandLine

@CommandLine.Command(name = "gitlab",
        description = "Gitlab API commander",
        subcommands = [Project.class])
class GitlabCLI extends AbstractCommand {

    GitlabAPI gitlabAPI

    @Override
    void run() {
        super.run()
        CommandLine.usage(this, System.out)
    }

//    void bootStrap() {
//        gitLabAPI = gitLabAPI ?: new GitlabAPI(metadata["gitlabURL"], metadata["gitlabPrivateToken"])
//    }
}