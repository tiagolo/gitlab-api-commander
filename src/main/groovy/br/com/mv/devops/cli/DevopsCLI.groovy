package br.com.mv.devops.cli

import br.com.mv.devops.cli.commands.BambooCLI
import br.com.mv.devops.cli.commands.GitlabCLI
import br.com.mv.devops.cli.commands.NexusCLI
import br.com.mv.devops.cli.commands.base.AbstractCommand
import br.com.mv.devops.cli.providers.ManifestVersionProvider
import com.atlassian.api.bamboo.BambooAPI
import groovy.json.JsonSlurper
import org.sonatype.nexus.api.NexusAPI
import picocli.CommandLine

@CommandLine.Command(name = "devops-cli",
        subcommands = [ GitlabCLI.class, BambooCLI.class, NexusCLI.class ],
        versionProvider = ManifestVersionProvider.class,
        description = "Command Line Interface for releasing software with git",
        header = [
                """\
  _____              ____            
 |  __ \\            / __ \\           
 | |  | | _____   _| |  | |_ __  ___ 
 | |  | |/ _ \\ \\ / / |  | | '_ \\/ __|
 | |__| |  __/\\ V /| |__| | |_) \\__ \\
 |_____/ \\___| \\_/  \\____/| .__/|___/
                          | |        
                          |_|        
"""
        ])
class DevopsCLI extends AbstractCommand {

    @CommandLine.Option(names = ["-v", "--version"], versionHelp = true, description = "display version info")
    boolean versionInfoRequested

    static void main(String[] args) {
        CommandLine.run(new DevopsCLI(), args)
    }

    @Override
    void run() {
        super.run()
        CommandLine.usage(this, System.out)
    }

    def loadJSONData(String fileName) {
        def data = new JsonSlurper().parse(new File("${metadata["workspace"]}/src/main/resources/$fileName"))
        return data
    }

    /**
     * *******************************************************************
     *      Acquiring Variables
     * *******************************************************************
     */

    DevopsCLI() {
        getMetadata()
    }

    def getMetadata() {

        def metadata = [
                workspace         : "",
                nexusURL          : "http://nexus.mvfor.local",
                bambooURL         : "https://bamboo.cloudmv.com.br",
                bambooToken       : "d933004d053ca6b566da8354ee941d20839790dc",
                gitlabURL         : "https://git.mv.com.br/api/v4",
                gitlabPrivateToken: "${System.getenv("gitlabPrivateToken")}",
                gitlabUsername    : "bamboo-integration",
                gitlabPassword    : "${System.getenv("gitlabPassword")}"
        ]

        metadata["workspace"] = getClass().protectionDomain.codeSource.location.path - ~/src\/.*/

        if (["/groovy/script"].any { metadata['workspace'].contains(it) })
            metadata["workspace"] = "/D:/MV/git/devops/bamboo/bamboo-plan-dsl"
        else if ((metadata["workspace"]).endsWith("out/production/classes/"))
            metadata["workspace"] -= "out/production/classes/"
        else
            logger.info(metadata["workspace"])

        return metadata
    }

    private void bootstrapGitlabAPI() {
        getMetadata()

        bambooAPI = bambooAPI ?: new BambooAPI(metadata["bambooURL"], metadata["bambooToken"])
        nexusAPI = nexusAPI ?: new NexusAPI(metadata["nexusURL"])
    }
}
