package br.com.mv.devops.api

import groovy.json.JsonBuilder

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class BaseAPI {

    BaseAPI() {
        ignoreSelfSignedCertificates()
    }

    /**
     * Recupera os parâmetros para a execução de APIs Rest através de POST
     *
     * @param map
     * @return string da lista de parâmetros unidos pelo caracter '&'
     */
    protected getMessageBody(map) {
        if (map instanceof Map)
            map.collect().join("&")
        else if (map instanceof JsonBuilder)
            map.content.collect().join("&")
        else
            map
    }
    /**
     * Executa uma chamada a uma API Rest
     *
     * @param endpoint - url completa da api a ser executada
     * @param method - método http (GET, POST, PUT, DELETE)
     * @param body - listas de parâmetros em string separados pelo caractere '&'
     * @return
     */
    protected String callRestApi(String endpoint, method, body = "") {
        def url = new URL(endpoint)
        def connection = url.openConnection()
        connection.setRequestMethod(method.toUpperCase())
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
        connection.doOutput = true
        def writer = new OutputStreamWriter(connection.outputStream)
        writer.write(body)
        writer.flush()
        writer.close()
        connection.connect()
        return connection.content.text
    }

    /**
     * @private
     * Ignora a validação de certificado SSL autoassinado.
     */
    protected void ignoreSelfSignedCertificates() {
        def nullTrustManager = [
                checkClientTrusted: { chain, authType -> },
                checkServerTrusted: { chain, authType -> },
                getAcceptedIssuers: { null }
        ]
        def nullHostnameVerifier = [
                verify: { hostname, session -> true }
        ]
        SSLContext sc = SSLContext.getInstance("SSL")
        sc.init(null, [nullTrustManager as X509TrustManager] as TrustManager[], null)
        HttpsURLConnection.setDefaultHostnameVerifier(nullHostnameVerifier as HostnameVerifier)
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
    }

}
