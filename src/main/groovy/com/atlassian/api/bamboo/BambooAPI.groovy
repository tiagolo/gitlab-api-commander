package com.atlassian.api.bamboo

import br.com.mv.devops.api.BaseAPI
import groovy.json.JsonBuilder
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpURLClient
import groovyx.net.http.Method
import groovyx.net.http.RESTClient

import static groovyx.net.http.ContentType.*

class BambooAPI extends BaseAPI {

    String bambooUrl;
    String bambooToken = 'd933004d053ca6b566da8354ee941d20839790dc'
    private RESTClient client


    BambooAPI(String bambooUrl, String bambooToken) {
        super()

        this.bambooUrl = bambooUrl
        this.bambooToken = bambooToken

        this.client = createRestClient("${bambooUrl}/rest/api/latest/")
    }

    private RESTClient createRestClient(String url) {
        def username = 'tiago.costa'
        def password = 'bfY!@Voh5'

        RESTClient client = new RESTClient(url)
        client.setProxy('192.168.254.11', 3128, 'http')
        client.auth.basic(username, password)
        client.headers = [
                'Accept'           : 'application/json',
                'X-Atlassian-Token': 'no-check'
        ]

        return client
    }

    def listBuildPlans() {
        this.client.get(path: 'plan', query: [
                os_authType  : 'basic',
                'max-results': '250',
        ]).data
    }

    def getBuildPlanBranches(String buildPlan) {
        this.client.get(path: "plan/${buildPlan}/branch", query: [
                os_authType  : 'basic',
                'max-results': '250',
        ]).data
    }

    def deleteBuildPlan(String buildPlan) {

        def resp = this.client.post(path: "/ajax/deleteChain.action", query: [
                os_authType: 'basic',
                buildKey   : buildPlan
        ])

//        def body = new JsonBuilder()
//        body {
//            buildKey buildPlan
//            atl_token bambooToken
//            save 'Confirm'
//        }
//        body = getMessageBody(body)
//
//        callRestApi("${bambooUrl}/ajax/confirmDeleteChain.action?buildKey=${buildPlan}", "GET")
//
//        def result = callRestApi("${bambooUrl}/ajax/deleteChain.action!doDelete",
//                "POST", body)


        println "Deleted build plan: ${buildPlan}"
    }
}
